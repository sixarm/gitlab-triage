RSpec.shared_context 'stubs' do
  let(:project_id) { 123 }

  let(:admin) do
    {
      state: 'active',
      id: 1,
      web_url: 'https://gitlab.com/root',
      name: 'Administrator',
      avatar_url: nil,
      username: 'root'
    }
  end

  let(:issues) { [issue] }

  let(:issue) do
    {
      state: 'open',
      description: 'issue description',
      author: {
        state: 'active',
        id: 18,
        web_url: 'https://gitlab.com/eileen.lowe',
        name: 'Alexandra Bashirian',
        avatar_url: nil,
        username: 'eileen.lowe'
      },
      milestone: {
        project_id: project_id,
        description: 'milestone description',
        state: 'closed',
        due_date: nil,
        iid: 2,
        created_at: '2016-01-04T15:31:39.996Z',
        title: 'v4.0',
        id: 17,
        updated_at: '2016-01-04T15:31:39.996Z'
      },
      project_id: project_id,
      assignees: [admin],
      assignee: admin,
      updated_at: '2016-01-04T15:31:51.081Z',
      closed_at: nil,
      closed_by: nil,
      id: 76,
      title: 'issue title',
      created_at: '2016-01-04T15:31:51.081Z',
      iid: 6,
      labels: %w[bug],
      user_notes_count: 1,
      due_date: '2016-07-22',
      web_url: "https://gitlab.com/example/example/issue/6",
      confidential: false,
      weight: nil,
      discussion_locked: false,
      time_stats: {
        time_estimate: 0,
        total_time_spent: 0,
        human_time_estimate: nil,
        human_total_time_spent: nil
      }
    }
  end
end
