### Policy title

( add a title to accurately describe the policy that you are referring to )

### New Policy/Policy Change/Policy Removal

( declare the type of policy change this is )

### Issues or Merge Requests Policy?

Issues/Merge Requests/both

### Policy Conditions

( declare the conditions for the related policy )

#### Example

- Label: bug
- not updated for over 6 months
- no associated milestone (unscheduled)
- state: open

### Policy Actions

( declare the actions for the related policy )

#### Example

- Add a comment
- Mention @markglenfletcher
- Label: "awaiting feedback" "auto updated"
